## Master's thesis


AGH University of Science and Technology in Krakow


Title: Remote recording of values of variables in algorithms implemented in FPGA.

#### Pin assignment for DE0-Nano board

| Description | FPGA pin | DE0-Nano pin |
| --- | --- | --- |
| LAN8720 REFCLK           | R9     | GPIO_1_IN1      |
| LAN8720 TX0              | T15    | GPIO_11         |
| LAN8720 TX1              | F13    | GPIO_10         |
| LAN8720 TXEN             | T13    | GPIO_13         |
| LAN8720 MDIO             | R10    | GPIO_111        |
| LAN8720 MDC              | R11    | GPIO_19         |
| LAN8720 RX0              | R13    | GPIO_14         |
| LAN8720 RX1              | T14    | GPIO_12         |
| LAN8720 CRS              | T12    | GPIO_15         |