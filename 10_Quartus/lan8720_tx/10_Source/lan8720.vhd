--------------------------------------------------------------------------------
-- Entity name:   lan8720
-- File mame:     lan8720.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Configurable Ethernet transceiver utilizing external
--              LAN8720 module.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity lan8720 is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;  -- Asynchronous reset
-- TX inputs
   TX_SEND        : in  std_logic;  -- Starts packet transmission on rising edge
   TX_DATA_IN     : in  std_logic_vector(31 downto 0); -- Word to be sent
   TX_WREN        : in  std_logic;  -- Write enable
-- RX inputs
   RX_EN          : in  std_logic;  -- Enable receiver
   RX_READ        : in  std_logic;  -- Retrieve new byte from RX buffer
-- Config inputs
   CFG_INIT       : in  std_logic;  -- Enable device initialization
   CFG_DATA_IN    : in  std_logic_vector(7 downto 0); -- Memory data input
   CFG_RAM_ADDR   : in  std_logic_vector(HEADER_ADDR_BITS-1 downto 0); -- Memory address
   CFG_WR_EN      : in  std_logic;  -- Memory write enable
-- LAN8720 physical IO
   CRS            : in  std_logic;  -- LAN8720 carrier sense output
   RXD            : in  std_logic_vector(1 downto 0); -- LAN8720 RX outputs
   TXEN           : out std_logic;  -- LAN8720 TXEN input
   TXD            : out std_logic_vector(1 downto 0);  -- LAN8720 TXD[1..0] inputs
-- TX outputs
   TX_EMPTY       : out std_logic;  -- Input buffer empty
   TX_BUSY        : out std_logic;  -- Transmitter busy
-- RX outputs
   RX_BYTE        : out std_logic_vector(7 downto 0); -- Output byte
   RX_NEW         : out std_logic;                    -- New byte received
-- Config outputs
   CFG_BUSY       : out std_logic;
   state          : out std_logic_vector(3 downto 0);
   byte_r         : out std_logic_vector(7 downto 0);
   m_out          : out std_logic_vector(7 downto 0);
   m_in           : out std_logic_vector(7 downto 0)
);
end lan8720;


architecture structural of lan8720 is
   signal en_hdr_sig             : std_logic;
   signal en_init_sig            : std_logic;
   signal hdr_end_sig            : std_logic;
   signal next_byte_sig          : std_logic;
   signal cfg_out_sig            : std_logic_vector(7 downto 0);
   signal cfg_busy_sig           : std_logic;
begin
   -- Concurrent outputs assignment
   CFG_BUSY       <= cfg_busy_sig;
   
   config:        entity work.config_subs
                  port map(
                  REF_CLK        => REF_CLK,
                  RESET          => RESET,
                  EN_HDR         => en_hdr_sig,
                  NEXT_BYTE      => next_byte_sig,
                  EN_INIT        => en_init_sig,
                  HDR_DATA_IN    => CFG_DATA_IN,
                  HDR_RAM_ADDR   => CFG_RAM_ADDR,
                  HDR_WR_EN      => CFG_WR_EN,
                  DATA_OUT       => cfg_out_sig,
                  HDR_END        => hdr_end_sig,
                  BUSY           => cfg_busy_sig
                  );

   transmitter:   entity work.lan8720_tx
                  port map(
                  REF_CLK        => REF_CLK,
                  RESET          => RESET,
                  SEND           => TX_SEND and not cfg_busy_sig,
                  DATA_IN        => TX_DATA_IN,
                  WRITE_EN       => TX_WREN,
                  HDR_IN         => cfg_out_sig,
                  HDR_END        => hdr_end_sig,
                  EN_HDR         => en_hdr_sig,
                  NEXT_BYTE      => next_byte_sig,
                  TXEN           => TXEN,
                  TXD            => TXD,
                  BUFF_EMPTY     => TX_EMPTY,
                  BUSY           => TX_BUSY
                  );

   receiver:      entity work.lan8720_rx
                  port map(
                  REF_CLK        => REF_CLK,
                  RESET          => RESET,
                  ENABLE         => RX_EN,
                  READ_FIFO      => RX_READ,
                  CRS            => CRS,
                  RXD            => RXD,
                  CONF_EN        => CFG_INIT,
                  CONF_IN        => cfg_out_sig,
                  FIFO_OUT       => RX_BYTE,
                  NEW_DATA       => RX_NEW,
                  EN_CONF        => en_init_sig,
                  state          => state,
                  m_out          => m_out,
                  m_in           => m_in,
                  byte_r         => byte_r
                  );
end structural;