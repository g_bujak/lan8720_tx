--------------------------------------------------------------------------------
-- Entity name:   eth_data_buffer
-- File mame:     eth_data_buffer.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Input 32-bit data buffer. 
-- Shifts out input values in a byte-by-byte manner and stores next value
-- to be sent.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity eth_data_buffer is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;  -- Asynchronous reset
   READ_EN        : in  std_logic;  -- Read enable, starts shifting out bytes
   WRITE_EN       : in  std_logic;  -- Write enable
   DATA_IN        : in  std_logic_vector(31 downto 0);   -- Data input
   NEXT_BYTE      : in  std_logic;  -- Request next byte
   BUFF_EMPTY     : out std_logic;  -- Buffer empty, input next value
   BYTE           : out std_logic_vector(7 downto 0)    -- Output byte
);
end eth_data_buffer;


architecture behavioral of eth_data_buffer is
   -- Buffered data, present and next state
   signal buff_reg, buff_next          : std_logic_vector(31 downto 0);
   -- Currently processed data
   signal data_reg, data_next          : std_logic_vector(31 downto 0);
   -- Byte counter
   signal byte_cnt_reg, byte_cnt_next  : unsigned(1 downto 0);
   -- Initialization flag
   signal init_reg, init_next          : std_logic;
   -- Present, next state of outputs
   signal empty_reg, empty_next        : std_logic;
   signal byte_reg, byte_next          : std_logic_vector(7 downto 0);
begin
   -- Concurrent outputs assignment
   BUFF_EMPTY     <= empty_reg;
   BYTE           <= byte_reg;

   -- Synchronous process
   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         buff_reg       <= (others => '0');
         data_reg       <= (others => '0');
         byte_cnt_reg   <= (others => '0');
         init_reg       <= '0';
         empty_reg      <= '1';
         byte_reg       <= (others => '0');
      elsif(rising_edge(REF_CLK)) then
         buff_reg       <= buff_next;
         data_reg       <= data_next;
         byte_cnt_reg   <= byte_cnt_next;
         init_reg       <= init_next;
         empty_reg      <= empty_next;
         byte_reg       <= byte_next;
      end if;
   end process synch;

   -- Combinational process
   comb: process(buff_reg, data_reg, byte_cnt_reg, init_reg, empty_reg, byte_reg,
                 READ_EN, WRITE_EN, DATA_IN, NEXT_BYTE)
   begin
      -- Default assignment
      buff_next      <= buff_reg;
      data_next      <= data_reg;
      byte_cnt_next  <= byte_cnt_reg;
      init_next      <= init_reg;
      empty_next     <= empty_reg;
      byte_next      <= byte_reg;

      -- Shifting out bytes
      if(READ_EN = '1') then
         if(init_reg = '0') then
            -- Initialization of buffer reader
            data_next      <= buff_reg;
            byte_cnt_next  <= (others => '0');
            init_next      <= '1';
            buff_next      <= (others => '0');
            empty_next     <= '1';
         else
            byte_next      <= data_reg(31 downto 24);
            if(NEXT_BYTE = '1') then
               if(byte_cnt_reg = 3) then
                  data_next      <= buff_reg;
                  byte_cnt_next  <= (others => '0');
                  buff_next      <= (others => '0');
                  empty_next     <= '1';
               else
                  data_next      <= data_reg(23 downto 0) & X"00";
                  byte_cnt_next  <= byte_cnt_reg + 1;
               end if;
            end if;
         end if;
      else
         init_next      <= '0';
      end if;

      -- Write buffer
      if(WRITE_EN = '1') then
         buff_next      <= DATA_IN;
         empty_next     <= '0';
      end if;
   end process comb;
end behavioral;
