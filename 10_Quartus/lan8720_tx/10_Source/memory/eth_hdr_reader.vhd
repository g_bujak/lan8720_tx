--------------------------------------------------------------------------------
-- Entity name:   lan8720_hdr_reader
-- File mame:     lan8720_hdr_reader.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Retrieves subsequent bytes of Ethernet/IP/UDP header from
-- memory.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity eth_hdr_reader is
port(
   REF_CLK        : in  std_logic;     -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;     -- Asynchronous reset
   ENABLE         : in  std_logic;     -- Enable
   NEXT_BYTE      : in  std_logic;     -- Request next byte
   HDR_END        : out std_logic;     -- Last byte of header on output
   RAM_ADDR       : out std_logic_vector(HEADER_ADDR_BITS-1 downto 0)  -- Address of current byte
);
end eth_hdr_reader;


architecture behavioral of eth_hdr_reader is
   -- Byte counter
   signal byte_cnt_reg, byte_cnt_next     : unsigned(HEADER_ADDR_BITS-1 downto 0);
   -- Present, next state of outputs
   signal hdr_end_reg, hdr_end_next       : std_logic;
begin
   -- Concurrent outputs assignment
   HDR_END        <= hdr_end_reg;
   RAM_ADDR       <= std_logic_vector(byte_cnt_reg);
   
   -- Synchronous process; update state variables
   synch: process(REF_CLK, RESET, ENABLE)
   begin
      if(RESET = '1' or ENABLE = '0') then
         byte_cnt_reg         <= (others => '0');
         hdr_end_reg          <= '0';
      elsif(rising_edge(REF_CLK)) then
         byte_cnt_reg         <= byte_cnt_next;
         hdr_end_reg          <= hdr_end_next;
      end if;
   end process synch;
   
   -- Control process
   control: process(byte_cnt_reg, hdr_end_reg,
                    NEXT_BYTE)
   begin
      -- Default assignment
      byte_cnt_next        <= byte_cnt_reg;
      hdr_end_next         <= hdr_end_reg;

      -- Signal end of header when last byte reached
      if(byte_cnt_reg = HEADER_BYTES-1) then
         hdr_end_next   <= '1';
      elsif(NEXT_BYTE = '1') then
      -- Retrieve next byte from RAM
         byte_cnt_next  <= byte_cnt_reg + 1;
      end if;
   end process control;
end behavioral;