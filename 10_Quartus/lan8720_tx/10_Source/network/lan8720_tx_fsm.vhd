--------------------------------------------------------------------------------
-- Entity name:   lan8720_tx_fsm
-- File mame:     lan8720_tx_fsm.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: LAN8720 Ethernet transmitter state machine. 
-- Handles enabling Ethernet, TCP/IP, UDP headers and data registers.
-- Controls enabling transmitter and read/write enable of FCS generator.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity lan8720_tx_fsm is
port(
   REF_CLK        : in  std_logic;    -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;    -- Asynchronous reset
   SEND           : in  std_logic;    -- Starts packet transmission on rising edge
   NEXT_BYTE      : in  std_logic;    -- Send next byte to transmitter
   PRE_END        : in  std_logic;   -- Reached last byte of preamble
   HDR_END        : in  std_logic;    -- Reached last byte of header register
   FCS_END        : in  std_logic;   -- Reached last byte of FCS
   BYTE           : in  std_logic_vector(7 downto 0); -- Byte being sent
   EN_FCS         : out std_logic;   -- Enable FCS calculation
   EN_TX          : out std_logic;   -- Enable transmitter
   REG_MUX        : out std_logic_vector(3 downto 0)   -- Enable external registers
);
end lan8720_tx_fsm;


architecture behavioral of lan8720_tx_fsm is
   type state_type is (idle, preamble, eth_header, ip_header, data, fcs);
   -- Current, next state
   signal state_reg, state_next                 : state_type;
   -- Packet size (read from IP header)
   signal size_reg, size_next                   : unsigned(15 downto 0);
   -- Counter, bytes of data sent
   signal byte_cnt_reg, byte_cnt_next           : unsigned(15 downto 0);
   -- Counter, bit pairs sent
   signal bpair_cnt_reg, bpair_cnt_next         : unsigned(1 downto 0);
   -- Present and next states of FSM outputs
   signal en_fcs_reg, en_fcs_next               : std_logic;
   signal en_tx_reg, en_tx_next                 : std_logic;
   signal reg_mux_reg, reg_mux_next             : std_logic_vector(3 downto 0);
begin
   -- Concurrent outputs assignment
   EN_FCS            <= en_fcs_reg;
   EN_TX             <= en_tx_reg;
   REG_MUX           <= reg_mux_reg;
   
   -- Synchronous process; update state variables
   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         state_reg         <= idle;
         size_reg          <= (others => '0');
         byte_cnt_reg      <= (others => '0');
         bpair_cnt_reg     <= (others => '0');
         en_fcs_reg        <= '0';
         en_tx_reg         <= '0';
         reg_mux_reg       <= (others => '0');
      elsif(rising_edge(REF_CLK)) then
         -- Write next state variables
         state_reg         <= state_next;
         size_reg          <= size_next;
         byte_cnt_reg      <= byte_cnt_next;
         bpair_cnt_reg     <= bpair_cnt_next;
         en_fcs_reg        <= en_fcs_next;
         en_tx_reg         <= en_tx_next;
         reg_mux_reg       <= reg_mux_next;
      end if;
   end process synch;
   
   -- State machine combinational logic process
   fsm_comb: process(state_reg, byte_cnt_reg, bpair_cnt_reg, en_fcs_reg, 
                     en_tx_reg, reg_mux_reg, size_reg,
                     SEND, PRE_END, HDR_END, NEXT_BYTE, BYTE)
   begin
      -- Default assignation
      state_next           <= state_reg;
      size_next            <= size_reg;
      byte_cnt_next        <= byte_cnt_reg;
      bpair_cnt_next       <= bpair_cnt_reg;
      en_fcs_next          <= en_fcs_reg;
      en_tx_next           <= en_tx_reg;
      reg_mux_next         <= reg_mux_reg;
      
      case state_reg is

      when idle =>         -- Transmitter inactive
         if(SEND = '1') then
            state_next           <= preamble;
            en_tx_next           <= '1';
            reg_mux_next         <= ENABLE_PREAMBLE_GEN;
         end if;

      when preamble =>     -- Sending preamble
         if(PRE_END = '1' and NEXT_BYTE = '1') then
            -- Last byte of preamble is being sent
            state_next           <= eth_header;
            reg_mux_next         <= ENABLE_HEADER_REG;
            byte_cnt_next        <= (others => '0');
            bpair_cnt_next       <= (others => '0');
         end if;

      when eth_header =>   -- Sending Ethernet header (MAC addresses, type)
         -- Enable FCS calculation a cycle before sending first byte of header
         if(bpair_cnt_reg = 2) then
            en_fcs_next          <= '1';
         else
            bpair_cnt_next       <= bpair_cnt_reg + 1;
         end if;

         if(NEXT_BYTE = '1') then
            -- Reset byte counter at start of IP header
            -- Allows to compare byte counter to packet size in IP header
            if(byte_cnt_reg = IP_HEADER_START_IDX - 2) then
               state_next           <= ip_header;
               byte_cnt_next        <= (others => '0');
            else
               byte_cnt_next        <= byte_cnt_reg + 1;
            end if;
         end if;

      when ip_header =>       -- Sending TCP/IP and UDP header
         if(NEXT_BYTE = '1') then
            byte_cnt_next        <= byte_cnt_reg + 1;
            -- Get packet size from IP header
            if(byte_cnt_reg = IP_HEADER_SIZE_UPPER_IDX + 1) then
               size_next            <= unsigned(BYTE & X"00");
            end if;
            if(byte_cnt_reg = IP_HEADER_SIZE_LOWER_IDX + 1) then
               size_next            <= unsigned(std_logic_vector(size_reg(15 downto 8)) & BYTE);
            end if;

            if(HDR_END = '1') then
               -- End of header reached, start sending data
               state_next           <= data;
               reg_mux_next         <= ENABLE_DATA_REG;
            end if;
         end if;

      when data =>         -- Sending user data
         if(byte_cnt_reg = size_reg and NEXT_BYTE = '1') then
            state_next           <= fcs;
            byte_cnt_next        <= (others => '0');
            reg_mux_next         <= ENABLE_FCS_REG;
            bpair_cnt_next       <= (others => '0');
         elsif(NEXT_BYTE = '1') then
            byte_cnt_next        <= byte_cnt_reg + 1;
         end if;

      when fcs =>          -- Sending FCS
         if(byte_cnt_reg = 4) then
            -- Disable transmitter exactly when last byte has been sent
            if(bpair_cnt_reg = 2) then
               state_next        <= idle;
               reg_mux_next      <= REG_MUX_IDLE;
               en_tx_next        <= '0';
            else
               bpair_cnt_next    <= bpair_cnt_reg + 1;
            end if;
         elsif(NEXT_BYTE = '1') then
            en_fcs_next          <= '0';
            byte_cnt_next        <= byte_cnt_reg + 1;
         end if;
      when others =>
         state_next <= idle;
      end case;
   end process fsm_comb;
end behavioral;
