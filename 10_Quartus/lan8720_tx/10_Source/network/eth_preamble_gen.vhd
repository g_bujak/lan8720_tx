--------------------------------------------------------------------------------
-- Entity name:   eth_preamble_gen
-- File mame:     eth_preamble_gen.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Ethernet preamble generator.
-- Sends 8 bytes X"55", X"55", X"55", X"55", X"55", X"55", X"55", X"D5"
-- when enabled.
-- Assumes bytes will be sent in order from left side, while individual bits
-- will be transmitted beginning with least significant bit.
-- NOTE: This circuit is clocked on falling edge of clock signal.
--         (RMII inputs on LAN8720 are sampled on rising edge)
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity eth_preamble_gen is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
                                    -- Clocked on falling edge
   RESET          : in  std_logic;  -- Asynchronous reset
   ENABLE         : in  std_logic;  -- Enable
   NEXT_BYTE      : in  std_logic;  -- Request next byte
   BYTE           : out std_logic_vector(7 downto 0); -- Output byte
   PRE_END        : out std_logic   -- Last byte of preamble on output
);
end eth_preamble_gen;


architecture behavioral of eth_preamble_gen is
   -- Byte counter
   signal byte_cnt_reg              : unsigned(2 downto 0);
begin
   -- Concurrent outputs assignment
   BYTE            <= X"D5"      when (byte_cnt_reg = 7 and ENABLE = '1') else
                      X"55"      when ENABLE = '1' else
                      X"00";
   PRE_END         <= '1'        when (byte_cnt_reg = 7 and ENABLE = '1') else
                      '0';
   
   -- Synchronous process
   synch: process(REF_CLK, RESET, ENABLE)
   begin
      if(RESET = '1' or ENABLE = '0') then
         byte_cnt_reg      <= (others => '0');
      elsif(falling_edge(REF_CLK)) then
         if(NEXT_BYTE = '1' and byte_cnt_reg /= 7) then
            byte_cnt_reg       <= byte_cnt_reg + 1;
         end if;
      end if;
   end process synch;
end behavioral;
  