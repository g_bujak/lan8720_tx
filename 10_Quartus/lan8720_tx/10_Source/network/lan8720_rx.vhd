--------------------------------------------------------------------------------
-- Entity name:   lan8720_rx
-- File mame:     lan8720_rx.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Basic Ethernet receiver utilizing LAN8720 module.
-- Receives UDP packets sent to this device.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity lan8720_rx is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;  -- Asynchronous reset
   ENABLE         : in  std_logic;  -- Enable receiver
   READ_FIFO      : in  std_logic;  -- Read next byte from FIFO
   CRS            : in  std_logic;  -- LAN8720 carrier sense output
   RXD            : in  std_logic_vector(1 downto 0); -- LAN8720 RX outputs
   CONF_EN        : in  std_logic;  -- Start configuration process
   CONF_IN        : in  std_logic_vector(7 downto 0); -- Config input
   FIFO_OUT       : out std_logic_vector(7 downto 0); -- Output FIFO byte
   NEW_DATA       : out std_logic;  -- New data available in FIFO
   EN_CONF        : out std_logic;   -- Enable config reader
   state          : out std_logic_vector(3 downto 0);
   byte_r         : out std_logic_vector(7 downto 0);
   m_out          : out std_logic_vector(7 downto 0);
   m_in           : out std_logic_vector(7 downto 0)
);
end lan8720_rx;


architecture structural of lan8720_rx is
   signal s_byte              : std_logic_vector(7 downto 0);
   signal s_byte_out          : std_logic_vector(7 downto 0);
   signal s_new_byte          : std_logic;
   signal s_new_byte_out      : std_logic;
   signal s_read_fifo_edge    : std_logic;
   signal s_empty             : std_logic;
begin
   -- Concurrent outputs assignment
   NEW_DATA    <= not s_empty;
   byte_r      <= s_byte;
   
   state_machine: entity work.lan8720_rx_fsm(behavioral)
                  port map(
                  REF_CLK        => REF_CLK,
                  RESET          => RESET,
                  ENABLE         => ENABLE,
                  BYTE           => s_byte,
                  CONF_EN        => CONF_EN,
                  CONF_IN        => CONF_IN,
                  NEW_BYTE       => s_new_byte,
                  CRS            => CRS,
                  BYTE_OUT       => s_byte_out,
                  NEW_BYTE_OUT   => s_new_byte_out,
                  EN_CONF        => EN_CONF,
                  state          => state,
                  m_out          => m_out,
                  m_in           => m_in
                  );

   byte_rx:       entity work.lan8720_byte_rx(behavioral)
                  port map(
                  REF_CLK        => REF_CLK,
                  RESET          => RESET,
                  CRS            => CRS,
                  RXD            => RXD,
                  BYTE           => s_byte,
                  NEW_BYTE       => s_new_byte
                  );

   read_edge:     entity work.edge_detector_pos(behavioral)
                  port map(
                  CLOCK        => REF_CLK,
                  SIG_INPUT    => READ_FIFO,
                  POS_EDGE     => s_read_fifo_edge
                  );

   output_fifo:   entity work.rx_data_fifo
                  port map(
                  aclr           => RESET,
                  clock          => REF_CLK,
                  data           => s_byte_out,
                  rdreq          => s_read_fifo_edge,
                  wrreq          => s_new_byte_out,
                  empty          => s_empty,
                  full           => open,
                  q		         => FIFO_OUT
                  );
end structural;
