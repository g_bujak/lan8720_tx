--------------------------------------------------------------------------------
-- Entity name:   eth_fcs_gen
-- File mame:     eth_fcs_gen.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: FCS generator dedicated for 2-bit wide RMII bus.
-- Generates FCS based on parallel Ethernet CRC32 algorithm.
-- FCS is initialized with value 0xFFFFFFFF.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.crc32_operations.all;


entity eth_fcs_gen is
port(
   REF_CLK        : in  std_logic; -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic; -- Asynchronous reset
   READ_EN        : in  std_logic; -- Read enable, starts shifting out bytes
   WRITE_EN       : in  std_logic; -- Write enable, starts calculating FCS
   TXD            : in  std_logic_vector(1 downto 0); -- RMII TXD signal
   NEXT_BYTE      : in  std_logic; -- Request next byte
   BYTE           : out std_logic_vector(7 downto 0); -- Output byte
   FCS_END        : out std_logic  -- Last byte of FCS on output
--   fcs_out        : out std_logic_vector(31 downto 0);
--   fcs_conv       : out std_logic_vector(31 downto 0)
);
end eth_fcs_gen;

architecture behavioral of eth_fcs_gen is
   type state_type is (idle, first_byte, sending);
   -- Present, current state
   signal state_reg, state_next           : state_type;
   -- Present, next state of FCS
   signal fcs_reg, fcs_next               : std_logic_vector(31 downto 0);
   -- Converted FCS (ready to be sent)
   signal fcs_conv_reg, fcs_conv_next     : std_logic_vector(31 downto 0);
   -- Present, next state of output FCS (the value which is actually sent)
   signal fcs_out_reg, fcs_out_next       : std_logic_vector(31 downto 0);
   -- Byte counter
   signal byte_cnt_reg, byte_cnt_next     : unsigned(1 downto 0);
   signal fcs_end_reg, fcs_end_next       : std_logic;
begin
   -- Concurrent outputs assignment
   FCS_END        <= fcs_end_reg;
--   fcs_out        <= fcs_reg;
--   fcs_conv       <= fcs_conv_reg;
   -- Output mux
   -- Provide first byte of FCS immediately when it is available, next bytes
   -- provided by combinational logic
   with state_reg select
      BYTE           <= fcs_conv_reg(31 downto 24) when first_byte,
                        fcs_out_reg(31 downto 24)  when others;
   
   -- Synchronous process, update state variables
   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         state_reg      <= idle;
         fcs_reg        <= (others => '1');  -- init with 0xFFFFFFFF
         fcs_conv_reg   <= (others => '0');
         fcs_out_reg    <= (others => '0');
         byte_cnt_reg   <= (others => '0');
         fcs_end_reg    <= '0';
      elsif(rising_edge(REF_CLK)) then
         state_reg      <= state_next;
         fcs_reg        <= fcs_next;
         fcs_conv_reg   <= fcs_conv_next;
         fcs_out_reg    <= fcs_out_next;
         byte_cnt_reg   <= byte_cnt_next;
         fcs_end_reg    <= fcs_end_next;
      end if;
   end process synch;
   
   -- CRC32 conversion process
   conv: process(fcs_next)
   begin
      fcs_conv_next        <= crc32_convert(fcs_next);
   end process conv;
   
   -- Combinational process
   comb: process(state_reg, fcs_reg, fcs_conv_reg, fcs_out_reg, byte_cnt_reg, fcs_end_reg,
                 READ_EN, WRITE_EN, TXD, NEXT_BYTE)
   begin
      -- Default assignment
      state_next     <= state_reg;
      fcs_next       <= fcs_reg;
      fcs_out_next   <= fcs_out_reg;
      byte_cnt_next  <= byte_cnt_reg;
      fcs_end_next   <= fcs_end_reg;
      
      -- CRC32 calculation
      if(WRITE_EN = '1') then
         fcs_next       <= crc32_twobit(TXD(0) & TXD(1), fcs_reg);
      end if;
      
      -- Reading process state machine
      case state_reg is
      when idle =>
         if(READ_EN = '1') then
            state_next     <= first_byte;
         end if;     
      when first_byte =>
         if(NEXT_BYTE = '1') then
            state_next        <= sending;
            fcs_out_next      <= fcs_conv_reg(23 downto 0) & X"00";
            byte_cnt_next     <= (others => '0');
         end if;
      when sending =>
         if(byte_cnt_reg = 2) then
            fcs_end_next      <= '1';
            if(NEXT_BYTE = '1') then
               state_next     <= idle;
               fcs_next       <= (others => '1');
               fcs_out_next   <= (others => '0');
               fcs_end_next   <= '0';
            end if;
         elsif(NEXT_BYTE = '1') then
            byte_cnt_next     <= byte_cnt_reg + 1;
            fcs_out_next      <= fcs_out_reg(23 downto 0) & X"00";
         end if;
      end case;
   end process comb;
end behavioral;