--------------------------------------------------------------------------------
-- Package name:  crc32_operations
-- File mame:     crc32_operations.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Functions used to calculate CRC32 error detecting codes for 
-- Ethernet FCS
-- Polynomial used: 0x04C11DB7
-- CRC should be initialized with value i_crc := 0xFFFFFFFF
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;


package crc32_operations is
   -- Calculates CRC32 bit-by-bit
   -- Implemented as serial linear feedback shift register (LFSR)
   -- Processes one bit of message at a time
   function crc32_bitwise(i_bit        : std_logic;
                          i_crc        : std_logic_vector(31 downto 0))
                        return std_logic_vector;
                       
  
   -- Calculates CRC32 with two bits of input message at time
   -- Suited for RMII
     function crc32_twobit(i_bits      : std_logic_vector(1 downto 0);
                          i_crc        : std_logic_vector(31 downto 0))
                        return std_logic_vector;
                        
   -- Converts CRC32 result to a value ready to be sent
   
   function crc32_convert(i_crc        : std_logic_vector(31 downto 0))
                        return std_logic_vector;
end crc32_operations;


package body crc32_operations is
   function crc32_bitwise(i_bit      : std_logic;
                          i_crc      : std_logic_vector(31 downto 0))
                        return std_logic_vector is
      variable do_xor   : std_logic;
      variable o_crc      : std_logic_vector(31 downto 0);
   begin
      -- Input bit xor-ed with shifted out bit from previous CRC
      do_xor := i_bit xor i_crc(31);
      
      o_crc(0)      :=    do_xor;
      o_crc(1)      :=    i_crc(0)     xor do_xor;
      o_crc(2)      :=    i_crc(1)     xor do_xor;
      o_crc(3)      :=    i_crc(2);   
      o_crc(4)      :=    i_crc(3)     xor do_xor;
      o_crc(5)      :=    i_crc(4)     xor do_xor;
      o_crc(6)      :=    i_crc(5);   
      o_crc(7)      :=    i_crc(6)     xor do_xor;
      o_crc(8)      :=    i_crc(7)     xor do_xor;
      o_crc(9)      :=    i_crc(8);
      o_crc(10)     :=    i_crc(9)     xor do_xor;
      o_crc(11)     :=    i_crc(10)    xor do_xor;
      o_crc(12)     :=    i_crc(11)    xor do_xor;
      o_crc(13)     :=    i_crc(12);
      o_crc(14)     :=    i_crc(13);
      o_crc(15)     :=    i_crc(14);
      o_crc(16)     :=    i_crc(15)    xor do_xor;
      o_crc(17)     :=    i_crc(16);
      o_crc(18)     :=    i_crc(17);
      o_crc(19)     :=    i_crc(18);
      o_crc(20)     :=    i_crc(19);
      o_crc(21)     :=    i_crc(20);
      o_crc(22)     :=    i_crc(21)    xor do_xor;
      o_crc(23)     :=    i_crc(22)    xor do_xor;
      o_crc(24)     :=    i_crc(23);
      o_crc(25)     :=    i_crc(24);
      o_crc(26)     :=    i_crc(25)    xor do_xor;
      o_crc(27)     :=    i_crc(26);
      o_crc(28)     :=    i_crc(27);
      o_crc(29)     :=    i_crc(28);
      o_crc(30)     :=    i_crc(29);
      o_crc(31)     :=    i_crc(30);
      
      return o_crc;
   end crc32_bitwise;
   
   function crc32_twobit(i_bits     : std_logic_vector(1 downto 0);
                         i_crc      : std_logic_vector(31 downto 0))
                    return std_logic_vector is
      variable o_crc      : std_logic_vector(31 downto 0);
   begin
      o_crc(0)      :=    i_crc(30)    xor i_bits(0);
      o_crc(1)      :=    i_crc(31)    xor i_crc(30)   xor i_bits(1)   xor i_bits(0);
      o_crc(2)      :=    i_crc(0)     xor i_crc(30)   xor i_crc(31)   xor i_bits(1)   xor i_bits(0);
      o_crc(3)      :=    i_crc(1)     xor i_crc(31)   xor i_bits(1);
      o_crc(4)      :=    i_crc(2)     xor i_crc(30)   xor i_bits(0);
      o_crc(5)      :=    i_crc(3)     xor i_crc(30)   xor i_crc(31)   xor i_bits(1)   xor i_bits(0);
      o_crc(6)      :=    i_crc(4)     xor i_crc(31)   xor i_bits(1);
      o_crc(7)      :=    i_crc(5)     xor i_crc(30)   xor i_bits(0);
      o_crc(8)      :=    i_crc(6)     xor i_crc(30)   xor i_crc(31)   xor i_bits(1)   xor i_bits(0);
      o_crc(9)      :=    i_crc(7)     xor i_crc(31)   xor i_bits(1);
      o_crc(10)     :=    i_crc(8)     xor i_crc(30)   xor i_bits(0);
      o_crc(11)     :=    i_crc(9)     xor i_crc(30)   xor i_crc(31)   xor i_bits(1)   xor i_bits(0);
      o_crc(12)     :=    i_crc(10)    xor i_crc(30)   xor i_crc(31)   xor i_bits(1)   xor i_bits(0);
      o_crc(13)     :=    i_crc(11)    xor i_crc(31)   xor i_bits(1);
      o_crc(14)     :=    i_crc(12);
      o_crc(15)     :=    i_crc(13);
      o_crc(16)     :=    i_crc(14)    xor i_crc(30)   xor i_bits(0);
      o_crc(17)     :=    i_crc(15)    xor i_crc(31)   xor i_bits(1);
      o_crc(18)     :=    i_crc(16);
      o_crc(19)     :=    i_crc(17);
      o_crc(20)     :=    i_crc(18);
      o_crc(21)     :=    i_crc(19);
      o_crc(22)     :=    i_crc(20)    xor i_crc(30)   xor i_bits(0);
      o_crc(23)     :=    i_crc(21)    xor i_crc(30)   xor i_crc(31)   xor i_bits(1)   xor i_bits(0);
      o_crc(24)     :=    i_crc(22)    xor i_crc(31)   xor i_bits(1);
      o_crc(25)     :=    i_crc(23);
      o_crc(26)     :=    i_crc(24)    xor i_crc(30)   xor i_bits(0);   
      o_crc(27)     :=    i_crc(25)    xor i_crc(31)   xor i_bits(1);
      o_crc(28)     :=    i_crc(26);
      o_crc(29)     :=    i_crc(27);
      o_crc(30)     :=    i_crc(28);
      o_crc(31)     :=    i_crc(29);
      
      return o_crc;
   end crc32_twobit;
   
   function crc32_convert(i_crc     : std_logic_vector(31 downto 0))
                        return std_logic_vector is
      variable crc_rev, o_crc       : std_logic_vector(31 downto 0);
   begin
      -- Reverse order of bits
      for i in 0 to 31 loop
         crc_rev(31-i) := i_crc(i);
      end loop;
      o_crc := crc_rev xor X"FFFFFFFF";
      -- Rearrange bytes in order as they are sent
      o_crc := o_crc(7 downto 0) & o_crc(15 downto 8) & o_crc(23 downto 16) & o_crc(31 downto 24);
      return o_crc;
   end crc32_convert;
      
end crc32_operations;