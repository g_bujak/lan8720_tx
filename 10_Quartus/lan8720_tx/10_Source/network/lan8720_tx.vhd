--------------------------------------------------------------------------------
-- Entity name:   lan8720_tx
-- File mame:     lan8720_tx.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Ethernet transmitter utilizing LAN8720 module.
-- Sends UDP packets over network to a device at predefined IP and MAC address.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity lan8720_tx is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;  -- Asynchronous reset
   SEND           : in  std_logic;  -- Starts packet transmission on rising edge
   DATA_IN        : in  std_logic_vector(31 downto 0); -- Word to be sent
   WRITE_EN       : in  std_logic;  -- Write enable
   HDR_IN         : in  std_logic_vector(7 downto 0); -- Header bytes input
   HDR_END        : in  std_logic;  -- Last byte of header flag
   EN_HDR         : out std_logic;  -- Enable header generator
   NEXT_BYTE      : out std_logic;  -- Request next byte of header
   TXEN           : out std_logic;  -- LAN8720 TXEN input
   TXD            : out std_logic_vector(1 downto 0);  -- LAN8720 TXD[1..0] inputs
   BUFF_EMPTY     : out std_logic;  -- Input buffer empty
   BUSY           : out std_logic   -- Transmitter busy
   -- byte_sent      : out std_logic_vector(7 downto 0)
   -- fcs_sent       : out std_logic_vector(7 downto 0);
   -- reg_mux        : out std_logic_vector(3 downto 0);
   -- fcs_ena        : out std_logic;
   -- next_byte      : out std_logic
   -- fcs_val        : out std_logic_vector(31 downto 0);
   -- fcs_conv       : out std_logic_vector(31 downto 0)
);
end lan8720_tx;


architecture structural of lan8720_tx is
   -- Busy state signals
   signal busy_sig                     : std_logic;
   -- Next byte signal
   signal next_byte_sig                : std_logic;
   -- Ending byte signals
   signal pre_end_sig                  : std_logic;
   signal hdr_end_sig                  : std_logic;
   signal fcs_end_sig                  : std_logic;
   -- Enable signals
   signal en_tx_sig                    : std_logic;
   signal en_byte_tx_sig               : std_logic;
   signal en_fcs_sig                   : std_logic;
   signal reg_mux_sig                  : std_logic_vector(REG_MUX_LENGTH-1 downto 0);
   -- Byte signals
   signal byte_sig                     : std_logic_vector(7 downto 0);
   signal pre_byte_sig                 : std_logic_vector(7 downto 0);
   signal hdr_byte_sig                 : std_logic_vector(7 downto 0);
   signal data_byte_sig                : std_logic_vector(7 downto 0);
   signal fcs_byte_sig                 : std_logic_vector(7 downto 0);
   -- Output signal
   signal txd_sig                      : std_logic_vector(1 downto 0);
begin
   -- fcs_ena     <= en_fcs_sig;
   -- next_byte   <= next_byte_sig;
   -- byte_sent   <= byte_sig;
   -- fcs_sent    <= fcs_byte_sig;
   -- reg_mux     <= reg_mux_sig;

   -- Outputs assignment
   TXD               <= txd_sig;
   BUSY              <= busy_sig;
   EN_HDR            <= reg_mux_sig(ENABLE_HEADER_REG_IDX);
   -- Next byte output used only for header generator
   NEXT_BYTE         <= next_byte_sig and reg_mux_sig(ENABLE_HEADER_REG_IDX);


   -- Concurrent logic
   hdr_byte_sig      <= HDR_IN;
   hdr_end_sig       <= HDR_END;
   busy_sig          <= en_byte_tx_sig;
   en_tx_sig         <= SEND and not busy_sig;

   state_machine:    entity work.lan8720_tx_fsm(behavioral)
                     port map(
                     REF_CLK     => REF_CLK,         
                     RESET       => RESET,
                     SEND        => en_tx_sig,
                     NEXT_BYTE   => next_byte_sig,
                     PRE_END     => pre_end_sig,
                     HDR_END     => hdr_end_sig,
                     FCS_END     => fcs_end_sig,
                     BYTE        => byte_sig,
                     EN_FCS      => en_fcs_sig,
                     EN_TX       => en_byte_tx_sig,
                     REG_MUX     => reg_mux_sig
                     );
                  
   byte_tx:          entity work.lan8720_byte_tx(behavioral)
                     port map(
                     REF_CLK     => REF_CLK,
                     RESET       => RESET,
                     ENABLE      => en_byte_tx_sig,
                     BYTE        => byte_sig,
                     NEXT_BYTE   => next_byte_sig,
                     TXEN        => TXEN,
                     TXD         => txd_sig
                     );
                     
   byte_mux:         entity work.lan8720_byte_mux(behavioral)
                     port map(
                     SEL         => reg_mux_sig,
                     PRE_IN      => pre_byte_sig,
                     HDR_IN      => hdr_byte_sig,
                     DATA_IN     => data_byte_sig,
                     FCS_IN      => fcs_byte_sig,
                     OUT_SEL     => byte_sig
                     );
                     
   preamble_gen:     entity work.eth_preamble_gen(behavioral)
                     port map(
                     REF_CLK     => REF_CLK,
                     RESET       => RESET,
                     ENABLE      => reg_mux_sig(ENABLE_PREAMBLE_GEN_IDX),
                     NEXT_BYTE   => next_byte_sig,
                     BYTE        => pre_byte_sig,
                     PRE_END     => pre_end_sig
                     );
                     
   -- header_gen:       entity work.eth_hdr_gen(structural)
   --                   port map(
   --                   REF_CLK     => REF_CLK,
   --                   RESET       => RESET,
   --                   ENABLE      => reg_mux_sig(ENABLE_HEADER_REG_IDX),
   --                   GEN_CHKSM   => en_chksm_gen_sig,
   --                   NEXT_BYTE   => next_byte_sig,
   --                   DATA_IN     => HDR_DATA_IN,
   --                   RAM_ADDR    => HDR_RAM_ADDR,
   --                   WR_EN       => HDR_WR_EN,
   --                   BYTE        => hdr_byte_sig,
   --                   HDR_END     => hdr_end_sig,
   --                   BUSY        => hdr_busy_sig
   --                   );
                     
   data_buffer:      entity work.eth_data_buffer(behavioral)
                     port map(
                     REF_CLK     => REF_CLK,
                     RESET       => RESET,
                     READ_EN     => reg_mux_sig(ENABLE_DATA_REG_IDX),
                     WRITE_EN    => WRITE_EN,
                     DATA_IN     => DATA_IN,
                     NEXT_BYTE   => next_byte_sig,
                     BUFF_EMPTY  => BUFF_EMPTY,
                     BYTE        => data_byte_sig
                     );
                     
   fcs_gen:          entity work.eth_fcs_gen(behavioral)
                     port map(
                     REF_CLK     => REF_CLK,
                     RESET       => RESET,
                     READ_EN     => reg_mux_sig(ENABLE_FCS_REG_IDX),
                     WRITE_EN    => en_fcs_sig,
                     TXD         => txd_sig,
                     NEXT_BYTE   => next_byte_sig,
                     BYTE        => fcs_byte_sig,
                     FCS_END     => fcs_end_sig
                     );
end structural;
