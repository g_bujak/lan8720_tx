--------------------------------------------------------------------------------
-- Entity name:   lan8720_rx_fsm
-- File mame:     lan8720_rx_fsm.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: LAN8720 Ethernet receiver state machine.
-- Receives UDP packets sent to this device.
-- Currently all packets with proper MAC address are read/
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity lan8720_rx_fsm is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;  -- Asynchronous reset
   ENABLE         : in  std_logic;  -- Enable receiving packets
   BYTE           : in  std_logic_vector(7 downto 0); -- Received byte
   CONF_EN        : in  std_logic;  -- Start configuration process
   CONF_IN        : in  std_logic_vector(7 downto 0); -- Config input
   NEW_BYTE       : in  std_logic;  -- New byte available on input
   CRS            : in  std_logic;  -- Carrier sense input
   BYTE_OUT       : out std_logic_vector(7 downto 0); -- UDP packet data
   NEW_BYTE_OUT   : out std_logic;  -- New byte available on output
   EN_CONF        : out std_logic;  -- Enable config reader
   state          : out std_logic_vector(3 downto 0);
   -- mem_addr       : out std_logic_vector(3 downto 0);
   m_out          : out std_logic_vector(7 downto 0);
   m_in           : out std_logic_vector(7 downto 0)
   -- byte_cnt       : out std_logic_vector(15 downto 0);
   -- size           : out std_logic_vector(15 downto 0)   
);
end lan8720_rx_fsm;


architecture behavioral of lan8720_rx_fsm is
   constant CONF_MEM_ADDR_BITS         : integer := 4;
   constant RAM_ACCESS_DELAY           : integer := 3;
   constant DEV_CONF_MEM_BYTES         : integer := 13;

-- Signals
   type state_type is (idle, preamble, mac_addr, wait_until_hdr,
                       header, udp_data, ignore, conf_read, ram_delay);
   -- Device configuration memory
   -- Stores MAC address
   type dev_conf_mem_type is array (0 to 2**CONF_MEM_ADDR_BITS - 1) 
      of std_logic_vector(7 downto 0);
   signal dev_conf_mem                 : dev_conf_mem_type := 
   (
      X"20", X"cf", X"30", X"4a", X"12", X"a9",
      X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00"
   );
   -- Config memory address
   signal mem_addr_reg, mem_addr_next  : unsigned(CONF_MEM_ADDR_BITS-1 downto 0);
   -- Config memory write enable
   signal wr_en_reg, wr_en_next        : std_logic;
   -- Config memory output
   signal mem_out                      : std_logic_vector(7 downto 0);
   -- Config memory input
   signal mem_in_reg, mem_in_next      : std_logic_vector(7 downto 0);
   -- Present, current state
   signal state_reg, state_next        : state_type;
   -- Byte counter
   signal byte_cnt_reg, byte_cnt_next  : unsigned(15 downto 0);
   -- Received byte
   signal rec_byte_reg, rec_byte_next  : std_logic_vector(7 downto 0);
   -- Packet size (read from IP header)
   signal size_reg, size_next          : unsigned(15 downto 0);
   -- Output registers
   signal byte_out_reg, byte_out_next  : std_logic_vector(7 downto 0);
   signal new_byte_reg, new_byte_next  : std_logic;
   signal en_conf_reg, en_conf_next    : std_logic;
begin
   -- Concurrent output assignment
   BYTE_OUT       <= byte_out_reg;
   NEW_BYTE_OUT   <= new_byte_reg;
   EN_CONF        <= en_conf_reg;
   with state_reg select
   state <= X"1" when idle,
            X"2" when preamble,
            X"3" when mac_addr,
            X"4" when wait_until_hdr,
            X"5" when header,
            X"6" when udp_data,
            X"7" when ignore,
            X"8" when conf_read,
            X"9" when ram_delay,
            X"F" when others;
   -- mem_addr <= std_logic_vector(mem_addr_reg);
   m_out <= mem_out;
   m_in  <= mem_in_reg;
   -- byte_cnt <= std_logic_vector(byte_cnt_reg);
   -- size <= std_logic_vector(size_reg);

   -- Config memory write process
   memory: process(REF_CLK)
   begin
      if(rising_edge(REF_CLK)) then
         -- Write memory
         if(wr_en_reg = '1') then
            dev_conf_mem(to_integer(mem_addr_reg)) <= mem_in_reg;
         end if;
      end if;
   end process memory;
   -- Memory read
   mem_out        <= dev_conf_mem(to_integer(mem_addr_reg));

   -- State machine synchronous process
   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         state_reg         <= idle;
         mem_addr_reg      <= (others => '0');
         wr_en_reg         <= '0';
         mem_in_reg        <= (others => '0');
         byte_cnt_reg      <= (others => '0');
         rec_byte_reg      <= (others => '0');
         size_reg          <= (others => '0');
         byte_out_reg      <= (others => '0');
         new_byte_reg      <= '0';
         en_conf_reg       <= '0';
      elsif(rising_edge(REF_CLK)) then
         state_reg         <= state_next;
         mem_addr_reg      <= mem_addr_next;
         wr_en_reg         <= wr_en_next;
         mem_in_reg        <= mem_in_next;
         byte_cnt_reg      <= byte_cnt_next;
         rec_byte_reg      <= rec_byte_next;
         size_reg          <= size_next;
         byte_out_reg      <= byte_out_next;
         new_byte_reg      <= new_byte_next;
         en_conf_reg       <= en_conf_next;
      end if;
   end process synch;

   -- State machine combinational logic
   comb: process(state_reg, mem_addr_reg, wr_en_reg, mem_out, mem_in_reg,
                 byte_cnt_reg, rec_byte_reg, size_reg, byte_out_reg, new_byte_reg,
                 ENABLE, BYTE, NEW_BYTE, CRS, CONF_EN, CONF_IN, en_conf_reg)
   begin
      -- Default assignment
      state_next        <= state_reg;
      mem_addr_next     <= mem_addr_reg;
      wr_en_next        <= wr_en_reg;
      mem_in_next       <= mem_in_reg;
      byte_cnt_next     <= byte_cnt_reg;
      rec_byte_next     <= rec_byte_reg;
      size_next         <= size_reg;
      byte_out_next     <= byte_out_reg;
      new_byte_next     <= new_byte_reg;
      en_conf_next      <= en_conf_reg;

      -- Receiving bytes
      if(NEW_BYTE = '1') then
         byte_cnt_next     <= byte_cnt_reg + 1;
         rec_byte_next     <= BYTE;
      end if;

      case state_reg is
      when idle =>   -- Receiver idle
         if(CONF_EN = '1') then
            state_next        <= ram_delay;
            mem_addr_next     <= (others => '0');
            en_conf_next      <= '1';
         elsif(NEW_BYTE = '1' and CRS = '1' and ENABLE = '1') then
            state_next        <= preamble;
            byte_cnt_next     <= (others => '0');
         end if;

      when ram_delay => -- Wait for duration of RAM access delay
         en_conf_next         <= '0';
         -- Use mem_addr_reg as counter
         if(mem_addr_reg = RAM_ACCESS_DELAY) then
            state_next        <= conf_read;
            mem_addr_next     <= (others => '0');
            wr_en_next        <= '1';
            mem_in_next       <= CONF_IN;
         else
            mem_addr_next     <= mem_addr_reg + 1;
         end if;

      when conf_read => -- Reading MAC configuration from RAM
         mem_addr_next     <= mem_addr_reg + 1;
         mem_in_next       <= CONF_IN;
         if(mem_addr_reg = DEV_CONF_MEM_BYTES-1) then
            state_next     <= ignore;  -- Ignore any packet that arrived in meantime
            wr_en_next     <= '0';
         end if;            

      when ignore => -- Ignoring packet when invalid or not the receiver
         if(CRS = '0') then
            state_next     <= idle;
         end if;
      
      when preamble =>  -- Receiving preamble
         -- Ignore packet if preamble is invalid
         if((byte_cnt_reg /= PREAMBLE_LENGTH-1 and rec_byte_reg /= PREAMBLE_BYTE)
               or (byte_cnt_reg = PREAMBLE_LENGTH-1 and rec_byte_reg /= PREAMBLE_END_BYTE)) then
            state_next        <= ignore;
         elsif(byte_cnt_reg = PREAMBLE_LENGTH-1 and NEW_BYTE = '1') then
         -- Next state when ending preamble byte received and next byte
         -- is about to be read
            state_next        <= mac_addr;
            mem_addr_next     <= (others => '0');
            byte_cnt_next     <= (others => '0');
         end if;

      when mac_addr =>  -- Reading receiver MAC address
         if(NEW_BYTE = '1') then
            -- Read next byte from memory
            mem_addr_next     <= mem_addr_reg + 1;
            if(byte_cnt_reg = RX_MAC_ADDR_END_IDX) then
               state_next     <= wait_until_hdr;
            end if;
         elsif(rec_byte_reg /= mem_out) then
            -- Ignore packet as we're not the recipient
            state_next        <= ignore;
         end if;

      when wait_until_hdr =>  -- Wait until start of IP header
         if(byte_cnt_reg = IP_HDR_START_IDX and NEW_BYTE = '1') then
            state_next        <= header;
            byte_cnt_next     <= (others => '0');
         end if;

      when header => -- Reading data from IP header
         -- Packet size
         if(byte_cnt_reg = IP_HEADER_SIZE_UPPER_IDX) then
            size_next            <= unsigned(std_logic_vector'(rec_byte_reg & X"00"));
         elsif(byte_cnt_reg = IP_HEADER_SIZE_LOWER_IDX) then
            size_next            <= unsigned(std_logic_vector'(std_logic_vector(size_reg(15 downto 8)) & rec_byte_reg));
         end if;

         -- Verifying IP address, packet type, port
         if(NEW_BYTE = '1' 
            and (byte_cnt_reg = IP_HEADER_PROTOCOL_IDX-1 
            or byte_cnt_reg = IP_HEADER_DEST_IP_IDX-1
            or byte_cnt_reg = IP_HEADER_DEST_IP_IDX
            or byte_cnt_reg = IP_HEADER_DEST_IP_IDX+1
            or byte_cnt_reg = IP_HEADER_DEST_IP_IDX+2
            or byte_cnt_reg = IP_HEADER_DEST_PORT_IDX-1
            or byte_cnt_reg = IP_HEADER_DEST_PORT_IDX)) then
            if(BYTE /= mem_out) then
               state_next     <= ignore;
            else
               mem_addr_next  <= mem_addr_reg + 1;
            end if;
         end if;

         -- UDP packet data start
         if(byte_cnt_reg = UDP_DATA_START_IDX) then
            state_next        <= udp_data;
         end if;

      when udp_data =>  -- Sending data to output
         new_byte_next  <= '0';
         if(byte_cnt_reg = size_reg-1) then
            state_next     <= ignore;     -- Wait until CRS goes to '0'
         elsif(NEW_BYTE = '1') then
            byte_out_next  <= BYTE;
            new_byte_next  <= '1';
         end if;

      when others =>
         state_next     <= idle;
      end case;
   end process comb;
end behavioral;
