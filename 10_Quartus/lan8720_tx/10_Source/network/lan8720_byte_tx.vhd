--------------------------------------------------------------------------------
-- Entity name:    lan8720_byte_tx
-- File mame:       lan8720_byte_tx.vhd
-- Device:         EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:         Grzegorz Bujak
--
-- Description: LAN8720 Ethernet RMII byte transmitter.
-- Handles LAN8720 TXEN and TXD inputs.
-- NOTE: This circuit is clocked on falling edge of clock signal.
--         (RMII inputs on LAN8720 are sampled on rising edge)
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity lan8720_byte_tx is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
                                    -- Clocked on falling edge
   RESET          : in  std_logic;  -- Asynchronous reset
   ENABLE         : in  std_logic;  -- Enable
   BYTE           : in  std_logic_vector(7 downto 0); -- Byte to send
   NEXT_BYTE      : out std_logic;  -- Request next byte from register
   TXEN           : out std_logic;  -- LAN8720 TXEN input
   TXD            : out std_logic_vector(1 downto 0)  -- LAN8720 TXD[1..0] inputs
);
end lan8720_byte_tx;


architecture behavioral of lan8720_byte_tx is
   -- Currently processed byte register; present, next state
   signal byte_reg, byte_next                : std_logic_vector(7 downto 0);
   -- Bit pair counter; present, next state
   signal bpair_cnt_reg, bpair_cnt_next      : unsigned(1 downto 0);
   -- Present, next states of outputs
   signal next_byte_reg, next_byte_next      : std_logic;
   signal txen_reg, txen_next                : std_logic;
begin
   -- Concurrent outputs assignment
   TXEN              <= txen_reg;
   NEXT_BYTE         <= next_byte_reg;
   TXD               <= byte_reg(1 downto 0);
   
   -- Synchronous process; update state variables
   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         txen_reg             <= '0';
         byte_reg             <= (others => '0');
         bpair_cnt_reg        <= (others => '0');
         next_byte_reg        <= '0';
      elsif(falling_edge(REF_CLK)) then
         txen_reg             <= txen_next;
         byte_reg             <= byte_next;
         bpair_cnt_reg        <= bpair_cnt_next;
         next_byte_reg        <= next_byte_next;
      end if;
   end process synch;
   
   -- Counter process
   control: process(BYTE, ENABLE, byte_reg, bpair_cnt_reg)
   begin
      if(ENABLE = '1') then
         txen_next         <= '1';
         -- Shift out last two bites from byte register
         byte_next         <= "00" & byte_reg(7 downto 2);   
         bpair_cnt_next    <= bpair_cnt_reg + 1;
         next_byte_next    <= '0';
         
         if(bpair_cnt_reg = 0) then
            -- Latch new byte
            byte_next         <= BYTE;
            next_byte_next    <= '1';
         elsif(bpair_cnt_reg = 3) then
            -- Explicitly reset counter
            bpair_cnt_next    <= (others => '0');
         end if;
      else
         txen_next            <= '0';
         byte_next            <= (others => '0');
         bpair_cnt_next       <= (others => '0');
         next_byte_next       <= '0';
      end if;
   end process control;
end behavioral;