--------------------------------------------------------------------------------
-- Entity name:   eth_hdr_ip_chksm
-- File mame:     eth_hdr_ip_chksm.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: IP header checksum generator. When activated, reads IP header
-- from header memory, calculates checksum and writes is back to memory.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity eth_hdr_ip_chksm is
port(
   REF_CLK        : in  std_logic;     -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;     -- Asynchronous reset
   ENABLE         : in  std_logic;     -- Enable
   Q_RAM          : in  std_logic_vector(7 downto 0); -- RAM data output
   WRITE_EN       : out std_logic;     -- RAM write enable
   DATA           : out std_logic_vector(7 downto 0); -- RAM data input
   ADDR           : out std_logic_vector(HEADER_ADDR_BITS-1 downto 0);   -- RAM address
   BUSY           : out std_logic
);
end eth_hdr_ip_chksm;


architecture behavioral of eth_hdr_ip_chksm is
   type state_type is (idle, init_mem, sum, carry, write_reg);
   -- Present, next state
   signal state_reg, state_next           : state_type;
   -- RAM address counter
   signal addr_cnt_reg, addr_cnt_next     : unsigned(HEADER_ADDR_BITS-1 downto 0);
   -- Checksum register
   signal checksum_reg, checksum_next     : unsigned(23 downto 0);
   -- Previous byte register
   signal prev_byte_reg, prev_byte_next   : std_logic_vector(7 downto 0);
   -- Write enable
   signal wr_en_reg, wr_en_next           : std_logic;
   -- Lower part of 16-bit word is being written
   signal lower_reg, lower_next           : std_logic;
   -- Data out
   signal data_reg, data_next             : std_logic_vector(7 downto 0);
begin
   -- Concurrent outputs assignment
   WRITE_EN       <= wr_en_reg;
   DATA           <= data_reg;
   ADDR           <= std_logic_vector(addr_cnt_reg);
   with state_reg select
   BUSY  <= '0' when idle,
            '1' when others;

   -- Synchronous process; update state variables
   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         state_reg      <= idle;
         addr_cnt_reg   <= (others => '0');
         checksum_reg   <= (others => '0');
         prev_byte_reg  <= (others => '0');
         wr_en_reg      <= '0';
         lower_reg      <= '0';
         data_reg       <= (others => '0');
      elsif(rising_edge(REF_CLK)) then
         state_reg      <= state_next;
         addr_cnt_reg   <= addr_cnt_next;
         checksum_reg   <= checksum_next;
         prev_byte_reg  <= prev_byte_next;
         wr_en_reg      <= wr_en_next;
         lower_reg      <= lower_next;
         data_reg       <= data_next;
      end if;
   end process synch;

   -- Combinational process
   comb: process(state_reg, addr_cnt_reg, checksum_reg, wr_en_reg, 
                 lower_reg, data_reg, prev_byte_reg,
                 ENABLE, Q_RAM)
   begin
      -- Default assignment
      state_next     <= state_reg;
      addr_cnt_next  <= addr_cnt_reg;
      checksum_next  <= checksum_reg;
      prev_byte_next <= prev_byte_reg;
      wr_en_next     <= wr_en_reg;
      lower_next     <= lower_reg;
      data_next      <= data_reg;

      case state_reg is
         when idle =>   -- Checksum generator idle
            if(ENABLE = '1') then
               state_next     <= init_mem;
               addr_cnt_next  <= to_unsigned(IP_HEADER_START_IDX, addr_cnt_next'length);
               checksum_next  <= (others => '0');
               prev_byte_next <= (others => '0');
               lower_next     <= '0';
            end if;

         when init_mem =>  -- Start incrementing memory address to offset access delay
            addr_cnt_next  <= addr_cnt_reg + 1;
            if(addr_cnt_reg = IP_HEADER_START_IDX + 1) then
               state_next  <= sum;
            end if;

         when sum =>    -- Addition of IP header 16-bit words
            addr_cnt_next     <= addr_cnt_reg + 1;
            -- End when reached UDP header address
            if(addr_cnt_reg = UDP_HEADER_START_IDX + 2) then
               state_next     <= carry;
            elsif(addr_cnt_reg /= IP_HEADER_CHKSUM_UPPER_IDX + 2
               and addr_cnt_reg /= IP_HEADER_CHKSUM_LOWER_IDX + 2) then
               -- Checksum field ignored during calculation (treated as 0)
               if(lower_reg = '1') then
                  lower_next     <= '0';
                  checksum_next  <= checksum_reg(23 downto 0)
                                    + unsigned(X"00" & prev_byte_reg & Q_RAM);
               else
                  lower_next     <= '1';
                  prev_byte_next <= Q_RAM;
               end if;
            end if;

         when carry =>  -- Addition of carried-out bits to control sum
            if(checksum_reg(23 downto 16) /= 0) then
               checksum_next     <= unsigned(X"00" & std_logic_vector(checksum_reg(15 downto 0)))
                                    + unsigned(X"0000" & std_logic_vector(checksum_reg(23 downto 16)));
            else
               state_next        <= write_reg;
               checksum_next     <= not checksum_reg; -- One's complement of sum
            end if;

         when write_reg => -- Writing checksum to header memory
            wr_en_next     <= '1';
            -- Writting upper checksum byte
            data_next      <= std_logic_vector(checksum_reg(15 downto 8));
            addr_cnt_next  <= to_unsigned(IP_HEADER_CHKSUM_UPPER_IDX, addr_cnt_next'length);
            -- Writing lower checksum byte
            if(addr_cnt_reg = IP_HEADER_CHKSUM_UPPER_IDX) then
               data_next      <= std_logic_vector(checksum_reg(7 downto 0));
               addr_cnt_next  <= to_unsigned(IP_HEADER_CHKSUM_LOWER_IDX, addr_cnt_next'length);
            elsif(addr_cnt_reg = IP_HEADER_CHKSUM_LOWER_IDX) then
               state_next     <= idle;
               wr_en_next     <= '0';
               data_next      <= (others => '0');
               addr_cnt_next  <= (others => '0');
            end if;   
         when others =>
            state_next     <= idle;
      end case;
   end process comb;
end behavioral;