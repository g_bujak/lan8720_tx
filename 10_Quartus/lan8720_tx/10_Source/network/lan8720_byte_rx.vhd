--------------------------------------------------------------------------------
-- Entity name:   lan8720_byte_rx
-- File mame:     lan8720_byte_rx.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: LAN8720 Ethernet RMII byte receiver.
-- Handles LAN8720 CRS and RXD outputs.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity lan8720_byte_rx is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;  -- Asynchronous reset
   CRS            : in  std_logic;  -- LAN8720 carrier sense output
   RXD            : in  std_logic_vector(1 downto 0); -- LAN8720 RX outputs
   BYTE           : out std_logic_vector(7 downto 0);  -- Byte read from PHY
   NEW_BYTE       : out std_logic   -- New byte latched on output (pulse)
);
end lan8720_byte_rx;

architecture behavioral of lan8720_byte_rx is
   -- Byte register, present and next state
   signal byte_reg, byte_next             : std_logic_vector(7 downto 0);
   -- Byte output register
   signal byte_out_reg, byte_out_next     : std_logic_vector(7 downto 0);
   -- Bit pair counter
   signal bpair_cnt_reg, bpair_cnt_next   : unsigned(1 downto 0);
   -- Synchronized flag (receiver synchronized with preamble start)
   signal sync_reg, sync_next             : std_logic;
   -- New byte pulse
   signal new_byte_reg, new_byte_next     : std_logic;
begin
   -- Concurrent outputs assignment
   BYTE           <= byte_out_reg;
   NEW_BYTE       <= new_byte_reg;

   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         byte_reg       <= (others => '0');
         byte_out_reg   <= (others => '0');
         bpair_cnt_reg  <= (others => '0');
         sync_reg       <= '0';
         new_byte_reg   <= '0';
      elsif(rising_edge(REF_CLK)) then
         byte_reg       <= byte_next;
         byte_out_reg   <= byte_out_next;
         bpair_cnt_reg  <= bpair_cnt_next;
         sync_reg       <= sync_next;
         new_byte_reg   <= new_byte_next;
      end if;
   end process synch;

   comb: process(byte_reg, byte_out_reg, bpair_cnt_reg, sync_reg, new_byte_reg,
                 CRS, RXD)
   begin
      byte_next         <= byte_reg;
      byte_out_next     <= byte_out_reg;
      bpair_cnt_next    <= bpair_cnt_reg;
      sync_next         <= sync_reg;
      new_byte_next     <= '0';

      if(CRS = '1') then
         -- Wait until "01" received
         -- CRS is asserted a few cycles before preamble appears
         if(sync_reg = '1' or RXD(0) = '1') then
            sync_next      <= '1';
            byte_next      <= RXD & byte_reg(byte_reg'high downto 2);
            bpair_cnt_next <= bpair_cnt_reg + 1;
            if(bpair_cnt_reg = 3) then
               new_byte_next     <= '1';
               byte_out_next     <= RXD & byte_reg(byte_reg'high downto 2);
               bpair_cnt_next    <= (others => '0');
            end if;
         end if;
      else
         bpair_cnt_next    <= (others => '0');
         sync_next         <= '0';
      end if;
   end process comb;
end behavioral;