--------------------------------------------------------------------------------
-- Entity name:   rx_conf_reader
-- File mame:     rx_conf_reader.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Reader of receiver configuration from header memory.
-- Reads device MAC address etc.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity rx_conf_reader is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   RESET          : in  std_logic;  -- Asynchronous reset
   ENABLE         : in  std_logic;  -- Enable reading configuration
   RAM_ADDR       : out std_logic_vector(HEADER_ADDR_BITS-1 downto 0); -- RAM address
   BUSY           : out std_logic   -- Busy signal
);
end rx_conf_reader;

architecture behavioral of rx_conf_reader is
   -- State register
   type state_type is (idle, mac_addr, protocol, ip_addr, port_number);
   signal state_reg, state_next        : state_type;
   -- RAM address counter
   signal addr_cnt_reg, addr_cnt_next  : unsigned(HEADER_ADDR_BITS-1 downto 0);
begin
   -- Concurrent output assignment
   RAM_ADDR       <= std_logic_vector(addr_cnt_reg);
   BUSY           <= '0' when state_reg = idle else
                     '1';

   synch: process(REF_CLK, RESET)
   begin
      if(RESET = '1') then
         state_reg         <= idle;
         addr_cnt_reg      <= (others => '0');
      elsif(rising_edge(REF_CLK)) then
         state_reg         <= state_next;
         addr_cnt_reg      <= addr_cnt_next;
      end if;
   end process synch;

   comb: process(ENABLE, state_reg, addr_cnt_reg)
   begin
      -- Default assignment
      state_next        <= state_reg;
      addr_cnt_next     <= addr_cnt_reg + 1;

      case state_reg is
      when idle =>
         addr_cnt_next     <= (others => '0');
         if(ENABLE = '1') then
            state_next        <= mac_addr;
            addr_cnt_next     <= to_unsigned(MAC_ADDR_START_IDX, addr_cnt_reg'length);
         end if;

      when mac_addr =>  -- Reading MAC address from memory
         if(addr_cnt_reg = MAC_ADDR_END_IDX) then
            state_next        <= protocol;
            addr_cnt_next     <= to_unsigned(PROTOCOL_ID_IDX, addr_cnt_reg'length);
         end if;

      when protocol =>  -- Reading used protocol
      -- Single byte, immediately go to next state
         state_next        <= ip_addr;
         addr_cnt_next     <= to_unsigned(RX_IP_ADDR_START_IDX, addr_cnt_reg'length);

      when ip_addr =>   -- Reading IP address
         if(addr_cnt_reg = RX_IP_ADDR_END_IDX) then
            state_next        <= port_number;
            addr_cnt_next     <= to_unsigned(PORT_NUMBER_START_IDX, addr_cnt_reg'length);
         end if;

      when port_number =>  -- Reading port number
         if(addr_cnt_reg = PORT_NUMBER_END_IDX) then
            state_next        <= idle;
         end if;
      end case;
   end process comb;
end behavioral;
