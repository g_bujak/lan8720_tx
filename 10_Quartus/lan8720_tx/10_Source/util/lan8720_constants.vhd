--------------------------------------------------------------------------------
-- Package name:  lan8720_constants
-- File mame:     lan8720_constants.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Constants used by LAN8720 Ethernet transmitter.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package lan8720_constants is
------------------------------------------------
-- Transmitted bytes mux constants
------------------------------------------------
   -- Length of state machine output REG_MUX
   constant REG_MUX_LENGTH             : integer := 4;
   -- Indexes of '1's for particular states of mux
   constant ENABLE_PREAMBLE_GEN_IDX    : integer := 0;
   constant ENABLE_HEADER_REG_IDX      : integer := 1;
   constant ENABLE_DATA_REG_IDX        : integer := 2;
   constant ENABLE_FCS_REG_IDX         : integer := 3;
   
   -- State machine output REG_MUX words
   constant REG_MUX_IDLE               : std_logic_vector(3 downto 0) := "0000";
   constant ENABLE_PREAMBLE_GEN        : std_logic_vector(3 downto 0) := "0001";
   constant ENABLE_HEADER_REG          : std_logic_vector(3 downto 0) := "0010";
   constant ENABLE_DATA_REG            : std_logic_vector(3 downto 0) := "0100";
   constant ENABLE_FCS_REG             : std_logic_vector(3 downto 0) := "1000";
   
------------------------------------------------
-- Packet settings
------------------------------------------------
   -- Starting index of IP header in packet memory
   constant IP_HEADER_START_IDX        : integer := 14;
   -- IP header - size field relative index
   constant IP_HEADER_SIZE_UPPER_IDX   : integer := 2;
   constant IP_HEADER_SIZE_LOWER_IDX   : integer := 3;
   -- IP header - protocol ID relative index
   constant IP_HEADER_PROTOCOL_IDX     : integer := 9;
   -- IP header - destination IP address rel index
   constant IP_HEADER_DEST_IP_IDX      : integer := 16;
   -- Destination port index relative to start of IP header
   constant IP_HEADER_DEST_PORT_IDX    : integer := 22;
   -- IP header - checksum byte absolute indexes
   constant IP_HEADER_CHKSUM_UPPER_IDX : integer := 24;
   constant IP_HEADER_CHKSUM_LOWER_IDX : integer := 25;
   -- UDP header - start index
   constant UDP_HEADER_START_IDX       : integer := 34;

   -- Size of transmitted packet (without 8-byte preamble and 4-byte FCS)
   constant PACKET_BYTES               : integer := 60;
   
   -- Size of Ethernet/IP/UDP header (without 8-byte preamble)
   constant HEADER_BYTES               : integer := 42;
   -- Ethernet header RAM addresing bits
   constant HEADER_ADDR_BITS           : integer := 6;

   constant PREAMBLE_LENGTH            : integer := 8;
   constant PREAMBLE_BYTE              : std_logic_vector(7 downto 0) := X"55";
   constant PREAMBLE_END_BYTE          : std_logic_vector(7 downto 0) := X"D5";
   constant RX_MAC_ADDR_END_IDX        : integer := 5;
   constant IP_HDR_START_IDX           : integer := 13;
   -- Following index relative to start of IP header
   constant UDP_DATA_START_IDX         : integer := 27;

   -- Device MAC address absolute index
   constant MAC_ADDR_START_IDX         : integer := 6;
   constant MAC_ADDR_END_IDX           : integer := 11;
   -- Protocol ID absolute index
   constant PROTOCOL_ID_IDX            : integer := 23;
   -- Recipient IP address absolute index
   constant RX_IP_ADDR_START_IDX       : integer := 26;
   constant RX_IP_ADDR_END_IDX         : integer := 29;
   -- Recipient port number absolute index
   constant PORT_NUMBER_START_IDX      : integer := 36;
   constant PORT_NUMBER_END_IDX        : integer := 37;

end lan8720_constants;


package body lan8720_constants is
end lan8720_constants;