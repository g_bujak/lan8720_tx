--------------------------------------------------------------------------------
-- Entity name:   lan8720_byte_mux
-- File mame:     lan8720_byte_mux.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Routes bytes from register enabled by state machine to
-- the transmitter.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lan8720_constants.all;


entity lan8720_byte_mux is
port(
   SEL                              : in  std_logic_vector(REG_MUX_LENGTH-1 downto 0);
   PRE_IN, HDR_IN, DATA_IN, FCS_IN  : in  std_logic_vector(7 downto 0);
   OUT_SEL                          : out std_logic_vector(7 downto 0)
);
end lan8720_byte_mux;


architecture behavioral of lan8720_byte_mux is
begin
   with SEL select
      OUT_SEL <=  PRE_IN               when ENABLE_PREAMBLE_GEN,
                  HDR_IN               when ENABLE_HEADER_REG,
                  DATA_IN              when ENABLE_DATA_REG,
                  FCS_IN               when ENABLE_FCS_REG,
                  (others => '0')      when others;
end behavioral;