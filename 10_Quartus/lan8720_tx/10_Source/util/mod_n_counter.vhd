--------------------------------------------------------------------------------
-- Entity name:   mod_n_counter
-- File mame:     mod_n_counter.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Simple mod-n counter acting as frequency divider.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mod_n_counter is
generic(
   N              : integer := 5;
   BITS           : integer := 8
);
port(
   CLK            : in  std_logic;
   RESET          : in  std_logic;
   CLK_DIV        : out std_logic
);
end mod_n_counter;


architecture behavioral of mod_n_counter is
   signal cnt_reg, cnt_next            : unsigned(BITS-1 downto 0);
   signal out_reg                      : std_logic;
begin
   CLK_DIV           <= out_reg;
   cnt_next          <= cnt_reg + 1;

   synch: process(CLK, RESET)
   begin
      if(RESET = '1') then
         cnt_reg        <= (others => '0');
         out_reg        <= '0';
      elsif(rising_edge(CLK)) then
         cnt_reg        <= cnt_next;
         if(cnt_reg = N) then
            cnt_reg     <= (others => '0');
            out_reg     <= not out_reg;
         end if;
      end if;
   end process synch;
end behavioral;