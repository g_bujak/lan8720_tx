--------------------------------------------------------------------------------
-- Entity name:   smi
-- File mame:     smi.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Serial Management Interface controller (utilized by 
-- LAN8720 IC). Allows writing and reading constrol and status registers.
-- Minimum clock period: 400 ns (2.5 MHz)
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity smi is
port(
   CLK         : in  std_logic;  -- MDC clock signal, max f = 2.5 MHz
   RESET       : in  std_logic;  -- Asynchronous reset
   ENABLE      : in  std_logic;  -- Trigger data transmission
   WR_EN       : in  std_logic;  -- Write enable
   PHY_ADDR    : in  std_logic_vector(4 downto 0);    -- PHY address of receiving devide
   REG_ADDR    : in  std_logic_vector(4 downto 0);    -- Register address
   DATA_IN     : in  std_logic_vector(15 downto 0);   -- Written data (only when WR_EN  ='1')
   BUSY        : out std_logic;                       -- Busy output
   DATA_OUT    : out std_logic_vector(15 downto 0);   -- Read data (only when WR_EN  ='0')
   MDC         : out std_logic;  -- SMI clock signal
   MDIO        : inout std_logic -- SMI bidirectional data port
);
end smi;


architecture behavioral of smi is
   type state_type is (idle, preamble, header, rec_turnaround, receiving, send_turnaround, sending);
   constant PREAMBLE_LENGTH                  : integer := 32;
   -- Indexes relative to first bit after preamble
   constant HEADER_END_IDX                   : integer := 14;
   constant TURNAROUND_END_IDX               : integer := 16;
   constant FRAME_END_IDX                    : integer := 32;

   -- Present, next state
   signal state_reg, state_next              : state_type;
   -- Bit counter
   signal bit_cnt_reg, bit_cnt_next          : unsigned(5 downto 0);
   -- Header register (2 start, 2 opcode, 5 PHY addr, 5 reg addr)
   signal hdr_reg, hdr_next                  : std_logic_vector(13 downto 0);
   -- Opcode
   signal op_code                            : std_logic_vector(1 downto 0);
   -- Write enable register
   signal wr_en_reg, wr_en_next              : std_logic;
   -- Data register
   signal data_reg, data_next                : std_logic_vector(15 downto 0);
   -- Output register
   signal mdio_reg, mdio_next                : std_logic;
   signal data_out_reg, data_out_next        : std_logic_vector(15 downto 0);
begin
   -- Concurrent outputs assignment
   MDC         <= not CLK;       -- Inverted clock supplied to PHY
   DATA_OUT    <= data_out_reg;
   with state_reg select
   BUSY        <= '0' when idle,
                  '1' when others;

   -- Bidirectional output control
   with state_reg select
   MDIO        <= mdio_reg when preamble|header|send_turnaround|sending,
                  'Z' when others;

   -- Op code decoder
   with WR_EN select
   op_code     <= "10" when '0',
                  "01" when others;

   -- Synchronous process; update state variables
   synch: process(CLK, RESET)
   begin
      if(RESET = '1') then
         state_reg         <= idle;
         bit_cnt_reg       <= (others => '0');
         hdr_reg           <= (others => '0');
         wr_en_reg         <= '0';
         data_reg          <= (others => '0');
         mdio_reg          <= '0';
         data_out_reg      <= (others => '0');
      elsif(rising_edge(CLK)) then
         state_reg         <= state_next;
         bit_cnt_reg       <= bit_cnt_next;
         hdr_reg           <= hdr_next;
         wr_en_reg         <= wr_en_next;
         data_reg          <= data_next;
         mdio_reg          <= mdio_next;
         data_out_reg      <= data_out_next;
      end if;
   end process synch;

   -- Combinational process
   comb: process(state_reg, bit_cnt_reg, hdr_reg, wr_en_reg, data_reg, mdio_reg,
                 data_out_reg, op_code,
                 ENABLE, WR_EN, PHY_ADDR, REG_ADDR, DATA_IN, MDIO)
   begin
      -- Default assignment
      state_next        <= state_reg;
      hdr_next          <= hdr_reg;
      wr_en_next        <= wr_en_reg;
      data_next         <= data_reg;
      mdio_next         <= mdio_reg;
      data_out_next     <= data_out_reg;

      -- Bit counter
      bit_cnt_next      <= bit_cnt_reg + 1;

      case state_reg is
      when idle =>   -- Interface idle
         if(ENABLE = '1') then
            state_next        <= preamble;
            bit_cnt_next      <= (others => '0');
            -- Create header of SMI frame to be sent
            -- "01" - start of frame
            hdr_next          <= "01" & op_code & PHY_ADDR & REG_ADDR;
            wr_en_next        <= WR_EN;
            data_next         <= DATA_IN;
            mdio_next         <= '1';  -- Start sending preamble
         end if;

      when preamble =>  -- Sending preamble (32 1's)
         mdio_next         <= '1';
         if(bit_cnt_reg = PREAMBLE_LENGTH-2) then
            state_next        <= header;
            bit_cnt_next      <= (others => '0');
         end if;

      when header =>    -- Sending header (start of frame, opcode, addresses)
         mdio_next         <= hdr_reg(hdr_reg'high);   -- Last bit of hdr_reg
         hdr_next          <= hdr_reg(hdr_reg'high-1 downto 0) & '0'; -- Shift to left
         if(bit_cnt_reg = HEADER_END_IDX) then
            if(wr_en_reg = '1') then
               state_next        <= send_turnaround;
            else
               state_next        <= rec_turnaround;
            end if;
         end if;

      when send_turnaround => -- Two cycles before sending data
         mdio_next         <= '0';
         if(bit_cnt_reg = TURNAROUND_END_IDX-1) then
            state_next     <= sending;
         end if;

      when sending =>   -- Sending data to be written to register
         mdio_next         <= data_reg(data_reg'high);  -- Last byte of data
         data_next         <= data_reg(data_reg'high-1 downto 0) & '0';
         if(bit_cnt_reg = FRAME_END_IDX) then
            state_next        <= idle;
         end if;

      when rec_turnaround =>  -- Turnaround before receving data
         if(bit_cnt_reg = TURNAROUND_END_IDX-1) then
            state_next        <= receiving;
         end if;

      when receiving => -- Receiving data
         if(bit_cnt_reg = FRAME_END_IDX-1) then
            state_next        <= idle;
            data_out_next     <= data_reg(data_reg'high-1 downto 0) & MDIO;
         else
            data_next         <= data_reg(data_reg'high-1 downto 0) & MDIO;
         end if;

      when others =>
         state_next     <= idle;
      end case;
   end process comb;
end behavioral;