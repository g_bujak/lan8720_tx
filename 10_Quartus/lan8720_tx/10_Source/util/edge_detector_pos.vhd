--------------------------------------------------------------------------------
-- Entity name:   edge_detector_pos
-- File mame:     edge_detector_pos.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Simple rising edge detector.
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity edge_detector_pos is
port(
   CLOCK               : in   std_logic;
   SIG_INPUT           : in   std_logic;
   POS_EDGE            : out  std_logic
);
end edge_detector_pos;


architecture behavioral of edge_detector_pos is
   -- Previous states of input
   signal input_past      : std_logic_vector(1 downto 0);
begin
   POS_EDGE <= (input_past(0)) and (not input_past(1));
   process(CLOCK, SIG_INPUT)
   begin
      if(rising_edge(CLOCK)) then
         input_past(1) <= input_past(0);
         input_past(0) <= SIG_INPUT;
      end if;
   end process;
end behavioral;
