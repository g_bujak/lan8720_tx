--------------------------------------------------------------------------------
-- Entity name:   de0_test_lan8720
-- File mame:     de0_test_lan8720.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: Testing circuit dedicated for DE0 Nano board.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity de0_test_lan8720 is
port(
   REF_CLK        : in  std_logic;  -- LAN8720 50 MHz clock from REF_CLK output
   SEND           : in  std_logic;  -- Starts packet transmission
   GEN_CHKSM      : in  std_logic;  -- Generates IP header checksum
   CRS            : in  std_logic;  -- LAN8720 CRS output
   RXD            : in  std_logic_vector(1 downto 0); -- LAN8720 RXD output
   TXEN           : out std_logic;  -- LAN8720 TXEN input
   TXD            : out std_logic_vector(1 downto 0);  -- LAN8720 TXD[1..0] inputs
   LED            : out std_logic_vector(7 downto 0)   -- Board LEDs
);
end de0_test_lan8720;


architecture structural of de0_test_lan8720 is
   signal start_edge          : std_logic;
   signal btn_edge            : std_logic;
begin
   start_detector:   entity work.edge_detector_neg
                     port map(
                     CLOCK          => REF_CLK,
                     SIG_INPUT      => SEND,
                     NEG_EDGE       => start_edge
                     );

   btn_detector:     entity work.edge_detector_neg
                     port map(
                     CLOCK          => REF_CLK,
                     SIG_INPUT      => GEN_CHKSM,
                     NEG_EDGE       => btn_edge
                     );

   lan8720:          entity work.lan8720
                     port map(
                     REF_CLK        => REF_CLK,
                     RESET          => '0',
                     TX_SEND        => start_edge,
                     TX_DATA_IN     => X"E003C007",
                     TX_WREN        => start_edge,
                     RX_EN          => '1',
                     RX_READ        => not SEND,
                     CFG_INIT       => btn_edge,
                     CFG_DATA_IN    => (others => '0'),
                     CFG_RAM_ADDR   => (others => '0'),
                     CFG_WR_EN      => '0',
                     CRS            => CRS,
                     RXD            => RXD,
                     TXEN           => TXEN,
                     TXD            => TXD,
                     TX_EMPTY       => open,
                     TX_BUSY        => open,
                     RX_BYTE        => LED,
                     RX_NEW         => open,
                     CFG_BUSY       => open
                     );
end structural;