--------------------------------------------------------------------------------
-- Entity name:   de0_test_smi
-- File mame:     de0_test_smi.vhd
-- Device:        EP4CE22F17C6
-- Software:      Quartus Prime 17.1.0 build 590 Lite Edition
-- Author:        Grzegorz Bujak
--
-- Description: SMI testing circuit dedicated for DE0 Nano board.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity de0_test_smi is
port(
   CLK            : in  std_logic;
   SEND           : in  std_logic;
   MUX_SEL        : in  std_logic;
   LED            : out std_logic_vector(7 downto 0);
   MDC            : out std_logic;
   MDIO           : inout  std_logic
);
end de0_test_smi;


architecture structural of de0_test_smi is
   signal send_edge        : std_logic;
   signal out_data         : std_logic_vector(15 downto 0);
   signal clk_div          : std_logic;
begin
   with MUX_SEL select
   LED      <= out_data(7 downto 0) when '1',
               out_data(15 downto 8) when others;

   freq_div:      entity work.mod_n_counter
                  generic map(24, 6)
                  port map(
                     CLK         => CLK,         
                     RESET       => '0',
                     CLK_DIV     => clk_div
                  );

   smi_dev:       entity work.smi
                  port map(
                     CLK         => clk_div,
                     RESET       => '0',
                     ENABLE      => send_edge,
                     WR_EN       => '0',
                     PHY_ADDR    => "00001",
                     REG_ADDR    => "00001",
                     DATA_IN     => X"0000",
                     BUSY        => open,
                     DATA_OUT    => out_data,
                     MDC         => MDC,
                     MDIO        => MDIO
                  );

   start_detector:   entity work.edge_detector_neg
                     port map(
                     CLOCK          => clk_div,
                     SIG_INPUT      => SEND,
                     NEG_EDGE       => send_edge
                     );
end structural;